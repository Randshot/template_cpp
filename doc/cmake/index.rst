CMake options
=============

Below is a list of all possible options that can be configured in CMake. Their
effect on the build and integrations with other options is also described.

In case you haven't used CMake before, there are three tools to configure:

* ``cmake``
	* command-line interface, have to specify all options at once
* ``ccmake``
	* curses interface, can see the options and select them
* ``cmake-gui``
	* Qt interface, can see the options and select them

.. toctree::
	:glob:

	*
