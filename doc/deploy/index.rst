Deploy
======

Documentation is deployed to both GitLab pages and a web host which accepts
``rsync`` to securely transfer data.

.. toctree::
	:glob:

	*
