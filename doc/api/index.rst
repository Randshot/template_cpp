API documentation
=================

API documentation is automatically generated from current sources. There are two
indexes available:

* `By filename <file/index.html>`_
* `By element name <file/genindex.html>`_
