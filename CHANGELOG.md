# Changelog

Reverse chronologically sorted, i.e. newest on top.

For details view the associated milestone of a release which are linked below.
View the commit history for minor fixes and improvements.

## 1.6.0 / 2018-07-08

https://git.mel.vin/template/c/milestones/12

## 1.5.3 / 2018-07-06

https://git.mel.vin/template/c/milestones/11

## 1.5.2 / 2018-06-27

https://git.mel.vin/template/c/milestones/10

## 1.5.1 / 2018-06-26

https://git.mel.vin/template/c/milestones/9

## 1.5.0 / 2018-06-21

https://git.mel.vin/template/c/milestones/8

## 1.4.0 / 2018-06-21

https://git.mel.vin/template/c/milestones/4

## 1.3.2 / 2018-06-15

https://git.mel.vin/template/c/milestones/7

## 1.3.1 / 2018-06-13

https://git.mel.vin/template/c/milestones/6

## 1.3.0 / 2018-06-11

https://git.mel.vin/template/c/milestones/5

## 1.2.0 / 2018-05-10

https://git.mel.vin/template/c/milestones/3

## 1.1.0 / 2017-11-27

https://git.mel.vin/template/c/milestones/2

## 1.0.0 / 2017-11-26

https://git.mel.vin/template/c/milestones/1
